﻿using System;
using System.Collections.Generic;

//this is the library added to get drives related stuff
using System.IO;


using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Working_With_Files_Drives_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //call the method that will do Drive stuff

            lets_do_drive_stuff();

            //putting this to stop the command line from dissapearing before I have had a chance to see the output

            Console.ReadLine();

        }

        //this is the method that will do drive stuff.

        private static void lets_do_drive_stuff()
        {
            //first step is to get all available information on the drives connected to PC
            //this would be a list of drives
            DriveInfo[] list_of_all_drives = DriveInfo.GetDrives();

            //now, looping through all the drives and doing something with them

            Console.WriteLine("Program.cs - lets_do_drive_stuff - Total Drives - {0}", list_of_all_drives.Length);
            Console.WriteLine("-------------------------------------------------------------" );

            foreach (DriveInfo temp_drive in list_of_all_drives)
            {
                //the object temp_drive will contain the current drive being looped through
                //in the list.

                //first, lets display the name of the drive
                Console.WriteLine("Program.cs - lets_do_drive_stuff - the current drive name is - {0}", temp_drive.Name);
                //second, lets display the type of drive
                Console.WriteLine("Program.cs - lets_do_drive_stuff - the file type is - {0}", temp_drive.DriveType);

                //now, i can display some more stuff, but I need to make sure that the drive is ready
                //if the drive is not ready, then display a message.
                
                //here, drive is indeed ready
                if (temp_drive.IsReady == true)
                {
                    Console.WriteLine("Program.cs - lets_do_drive_stuff - volume label is - {0}", temp_drive.VolumeLabel);
                    Console.WriteLine("Program.cs - lets_do_drive_stuff - file system is - {0}", temp_drive.DriveFormat);
                    Console.WriteLine("Program.cs - lets_do_drive_stuff - available space to current user is - {0}", temp_drive.AvailableFreeSpace);
                    Console.WriteLine("Program.cs - lets_do_drive_stuff - total available space is - {0}", temp_drive.TotalFreeSpace);
                    Console.WriteLine("Program.cs - lets_do_drive_stuff - total drive size is - {0}", temp_drive.TotalSize);
                }
                else //drive is not ready at all
                {
                    Console.WriteLine("Program.cs - lets_do_drive_stuff - the current drive {0} is just not ready", temp_drive.Name);
                }
                Console.WriteLine("-------------------------------------------------------------");
            }

            
        }
    }
}

















